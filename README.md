start docker: 
- docker-compose up

###### new migration ---> add migration file and add script in package.json
###### run migration ---> enter server container bash and run script

###### project has postman collection
###### routes:
- GET /user
- GET /user/:id
- POST /user
- UPDATE /user/:id
- DELETE /user/:id

###### use new database ---> delete ./pg_data, run docker, in server container bash run scripts:
- createdb (create database)
- migrate1 (create user table)
- migrate2 (create role table, add relation, input data)
- all routes works after migrate2

## Pg review

Pg is a driver for postgresql database. It is a base for almost all ORMs that will work with Postgres
[Docs of pg](https://node-postgres.com/)

## Homework
You will need to use this repo as a starter and implement following.
 - database and server should start with docker-compose.
 - server must have endpoints for user opertaions (create a user, delete user, update, view user list);
 - all created info should be stored in DB
 - write a script in migrations folder to create user table with required fields and a script in same section of package json file to run migration before start

