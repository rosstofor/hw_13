// TODO: create a router and bind endpoints here
import Router from "koa-router";
import {
  createUser,
  deleteUser,
  getUser,
  getUsers,
  updateUser,
} from "./controller";

const router = new Router({ prefix: "/user" });

router
  .get("/", getUsers)
  .get("/:id", getUser)
  .post("/", createUser)
  .put("/:id", updateUser)
  .delete("/:id", deleteUser);

export default router;
