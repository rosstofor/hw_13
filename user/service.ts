// TODO: implement db calls
import pool from "../db";
import { IUser } from "../db/user";

export const getUsersService = async () => {
  try {
    const data = await pool.query(`
            SELECT id, name, age, sex, privilegy, role_id FROM "user" ORDER BY id
        `);
    return data.rows;
  } catch (err) {
    return err;
  }
};

export const getUserService = async (id: string) => {
  try {
    const data = await pool.query(
      `SELECT id, name, age, sex, privilegy, role_id FROM "user" WHERE id=$1`,
      [id]
    );
    return data.rows;
  } catch (err) {
    return err;
  }
};

export const createUserService = async (user: IUser) => {
  try {
    const data = await pool.query(
      `INSERT INTO "user"(name, age, sex) VALUES($1,$2,$3)`,
      [user.name, user.age, user.sex]
    );
    return data.rows;
  } catch (err) {
    return err;
  }
};

export const checkUserService = async (id: string) => {
  try {
    const candidate = await pool.query(`SELECT id FROM "user" WHERE id=$1`, [
      id,
    ]);
    return candidate.rows;
  } catch (err) {
    return err;
  }
};

export const updateUserService = async (user: IUser, id: string) => {
  try {
    const data = await pool.query(
      `UPDATE "user" SET name=$1, age=$2, sex=$3 WHERE id=$4`,
      [user.name, user.age, user.sex, id]
    );
    return data.rows;
  } catch (err) {
    return err;
  }
};

export const deleteUserService = async (id: string) => {
  try {
    const data = await pool.query(`DELETE FROM "user" WHERE id=$1`, [id]);
    return data.rows;
  } catch (err) {
    return err;
  }
};
