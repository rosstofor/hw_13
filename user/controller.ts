// todo: implement service calls and write function to handle routes
import { Context } from "koa";
import { StatusCodes } from "../db/statusCodes";
import { IUser } from "../db/user";
import {
  checkUserService,
  createUserService,
  deleteUserService,
  getUserService,
  getUsersService,
  updateUserService,
} from "./service";

export const getUsers = async (ctx: Context) => {
  try {
    const data = await getUsersService();
    ctx.response.body = { success: true, data };
    ctx.response.status = StatusCodes.OK;
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = StatusCodes.BAD_REQUEST;
  }
};

export const getUser = async (ctx: Context) => {
  try {
    const id = ctx.request.url.split("/").pop();

    const data = await getUserService(id!);

    if (!data.length)
      return (ctx.response.body = { message: `User with id: ${id} not exist` });

    ctx.response.body = { success: true, data };
    ctx.response.status = StatusCodes.OK;
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = StatusCodes.BAD_REQUEST;
  }
};

export const createUser = async (ctx: Context) => {
  try {
    const user: IUser = {
      name: ctx.request.body.name,
      age: ctx.request.body.age,
      sex: ctx.request.body.sex,
    };
    const data = await createUserService(user);
    ctx.response.body = { success: true, data };
    ctx.response.status = StatusCodes.CREATED;
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = StatusCodes.BAD_REQUEST;
  }
};

export const updateUser = async (ctx: Context) => {
  try {
    const id = ctx.request.url.split("/").pop();

    const candidate = await checkUserService(id!);
    if (!candidate.length)
      return (ctx.response.body = { message: `User with id: ${id} not exist` });

    const user: IUser = {
      name: ctx.request.body.name,
      age: ctx.request.body.age,
      sex: ctx.request.body.sex,
    };
    const data = await updateUserService(user, id!);
    ctx.response.body = { success: true, data };
    ctx.response.status = StatusCodes.OK;
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = StatusCodes.BAD_REQUEST;
  }
};

export const deleteUser = async (ctx: Context) => {
  try {
    const id = ctx.request.url.split("/").pop();

    const candidate = await checkUserService(id!);
    if (!candidate.length)
      return (ctx.response.body = { message: `User with id: ${id} not exist` });

    const data = await deleteUserService(id!);
    ctx.response.body = { success: true, data };
    ctx.response.status = StatusCodes.OK;
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = StatusCodes.BAD_REQUEST;
  }
};
