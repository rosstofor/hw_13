require("dotenv").config();
import Koa from "koa";
import bodyParser from "koa-bodyparser";
import userRouter from './user/router'

const port = process.env.SERVER_PORT;

const app = new Koa();

app.use(bodyParser());
app.use(userRouter.routes())

app.listen(port, () => {
  console.log(`started ${port}`);
});
