export enum StatusCodes {
    OK = 200,
    CREATED = 201,
    BAD_REQUEST = 404
}