// add "role" table, input "role data", update "user" table
import { Pool } from "pg";
import databaseConfig from "../../config/database";

const pool = new Pool(databaseConfig);

pool.query(
   `create table if not exists "role"(
        id serial primary key,
        role varchar(10) not null default 'user' unique);
    insert into role(role) values('admin'),('user');
    alter table "user" add role_id int not null default 2;`,
      
  (err: any, res: any) => {
    if (err) console.log(err);
    pool.end();
  }
);
