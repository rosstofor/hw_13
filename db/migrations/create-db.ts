require("dotenv").config();
import { Pool } from "pg";

const DB_NAME = "hw_13";
const ADMIN_DB = "postgres";

const postgresConfig = {
  user: process.env.PGUSER,
  host: process.env.PGHOST,
  database: ADMIN_DB,
  password: process.env.PGPASSWORD,
  // @ts-ignore
  port: +process.env.PGPORT,
};

const pool = new Pool(postgresConfig);
//re-create database
// pool.query(`drop database  if exists ${DB_NAME}`, (err: any, res: any) => {
//   if (err) console.log(err);

//   pool.query(`create database ${DB_NAME}`, (err: any, res: any) => {
//     if (err) console.log(err);
//     pool.end();
//   });
// });

pool.query(`create database ${DB_NAME}`, (err: any, res: any) => {
  if (err) return console.log(err);
  console.log("database created");

  pool.end();
});
