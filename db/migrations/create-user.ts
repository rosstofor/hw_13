// TODO: write here sql scripts for initial execution (create user table for ex.)
import { Pool } from "pg";
import databaseConfig from "../../config/database";

const pool = new Pool(databaseConfig);

pool.query(
  `create table if not exists \"user\"(
      id serial primary key,
      name varchar(50) not null unique, 
      age int not null,
      sex varchar(10) not null,
      privilegy boolean default false not null)`,
      
  (err: any, res: any) => {
    if (err) console.log(err);
    pool.end();
  }
);
